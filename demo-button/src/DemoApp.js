import { html, css, LitElement } from 'lit-element';
import {styles} from './DemoApp.styles'
import '../demo-button';

export class DemoApp extends LitElement {

  static get properties() {
    return {
      backgroundColor: {
        type: String
      }
    }
  }

  constructor() {
    super();
    this.backgroundColor = 'gray';
    console.log(this.backgroundColor);
  }

  static get styles() {
    return [styles, css`
      :host {
        display: block;
      }
    `];
  }

  render() {
    return html`
      <div class="app" style="background-color: ${this.backgroundColor};">
        <h1>Welcome to Qualogy</h1>${this.backgroundColor}
        <demo-button></demo-button>
      </div>
    `;
  }
}
