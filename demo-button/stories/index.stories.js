import { storiesOf, html, withKnobs, withClassPropertiesKnobs } from '@open-wc/demoing-storybook';

import { DemoButton } from '../src/DemoButton.js';
import '../demo-button.js';

storiesOf('demo-button', module)
  .addDecorator(withKnobs)
  .add('Documentation', () => withClassPropertiesKnobs(DemoButton))
  .add(
    'Alternative Title',
    () => html`
      <demo-button .title=${'Something else'}></demo-button>
    `,
  );
