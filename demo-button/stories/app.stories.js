import { storiesOf, html, withKnobs, withClassPropertiesKnobs } from '@open-wc/demoing-storybook';

import { DemoApp } from '../src/DemoApp.js';
import '../demo-app.js';

storiesOf('demo-button', module)
  .addDecorator(withKnobs)
  .add('App', () => withClassPropertiesKnobs(DemoApp))
  .add(
    'Alternative Title',
    () => html`
      <demo-app .backgroundColor=${'green'}></demo-app>
    `,
  );
