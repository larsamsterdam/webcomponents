# \<demo-qualogy>

This webcomponent follows the [open-wc](https://github.com/open-wc/open-wc) recommendation.

## Installation
```bash
npm i demo-qualogy
```

## Usage
```html
<script type="module">
  import 'demo-qualogy/demo-qualogy.js';
</script>

<demo-qualogy></demo-qualogy>
```

## Testing using karma (if applied by author)
```bash
npm run test
```

## Testing using karma via browserstack (if applied by author)
```bash
npm run test:bs
```

## Demoing using storybook (if applied by author)
```bash
npm run storybook
```

## Linting (if applied by author)
```bash
npm run lint
```
