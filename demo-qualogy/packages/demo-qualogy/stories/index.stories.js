import { storiesOf, html, withKnobs, withClassPropertiesKnobs } from '@open-wc/demoing-storybook';

import { DemoQualogy } from '../src/DemoQualogy.js';
import '../demo-qualogy.js';

storiesOf('demo-qualogy', module)
  .addDecorator(withKnobs)
  .add('Documentation', () => withClassPropertiesKnobs(DemoQualogy))
  .add(
    'Alternative Title',
    () => html`
      <demo-qualogy .title=${'Something else'}></demo-qualogy>
    `,
  );
