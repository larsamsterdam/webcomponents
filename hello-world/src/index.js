class HelloWorld extends HTMLElement {
    constructor() {
        super();
        this.attachShadow({mode: 'open'});
    }
    connectedCallback() {
        console.log('Hello world');
    }
}

customElements.define('hello-world', HelloWorld);