import { storiesOf, html, withKnobs, withClassPropertiesKnobs } from '@open-wc/demoing-storybook';

import { DemoApp } from '../src/DemoApp.js';
import '../demo-app.js';

storiesOf('demo-app', module)
  .addDecorator(withKnobs)
  .add('Documentation', () => withClassPropertiesKnobs(DemoApp))
  .add(
    'Alternative Title',
    () => html`
      <demo-app .title=${'Something else'}></demo-app>
    `,
  );
